
Projeto View Covid-19

## Começando
Para executar:

Clone o repositório do github.com:
```
$ git clone https://gitlab.com/gableisure-dev/prj-view-covid.git
```

Navegue até o diretório view-covid:
```
$ cd covid/view-covid
```

Instale todos os requisitos do projeto:
 ```
$ npm install
 ```

Inicie o servidor:
 ```
$ npm run serve
 ```

